package ru.nsu.fit.g14201.lipatkin;

/**
 * Created by castiel on 13.02.2017.
 */
public enum TerminalSymbols {
    LeftBracket,
    RightBracket,
    Number,
    Plus,
    Minus,
    Division,
    Multiplication,
    Involution
}
