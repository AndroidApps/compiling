package ru.nsu.fit.g14201.lipatkin;

/**
 * Created by castiel on 14.02.2017.d
 */
public class ParseException extends CalculatorException {
    public String getMessage() {
        return "Parse Exception : wrong expression";
    }
}
