package ru.nsu.fit.g14201.lipatkin;

import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;

/**
 * Hello worldd!
 *
 */
public class App 
{
    static private final Logger log = Logger.getLogger(App.class.getName());

    public static void main( String[] args )
    {
        try {
            FileReader input = new FileReader("input.txt");
            Calculator calculator = new Calculator();
            System.out.println(calculator.calculate(input));
        } catch(IOException exp) {
            log.error(exp.getMessage());
        }
    }
}
