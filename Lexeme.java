package ru.nsu.fit.g14201.lipatkin;

/**
 * Created by castiel on 13.02.2017.d
 */
public class Lexeme {
    private TerminalSymbols symbol;
    private int value;

    public Lexeme(TerminalSymbols sym) {
        symbol = sym;
    }

    public Lexeme(TerminalSymbols sym, int val) {
        symbol = sym;
        value = val;
    }

    final TerminalSymbols getSymbol() { return symbol; }
    int getValue() { return value; }
}
