package ru.nsu.fit.g14201.lipatkin;

import java.io.Reader;

/**
 * Created by castiel on 13.02.2017.d
 */
public class Calculator {
    public int calculate(Reader reader) {
        Lexer lexer = new Lexer(reader);
        Parser parser = new Parser(lexer);

//        int result = 0;
//        Lexeme lexeme = lexer.nextLexeme();
//        switch (lexeme.getSymbol()) {
//            case LeftBracket:
//                result = parser.parseExpression();
//                break;
//            case Number:
//                result = lexeme.getValue();
//                break;
//            default:
//                throw new WrongSymbolLexemeException();
//        }

        return parser.parseExpression();
    }
}
