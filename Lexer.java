package ru.nsu.fit.g14201.lipatkin;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Reader;

/**
 * Created by castiel on 13.02.2017.d
 */
public class Lexer {
    private final Logger log = Logger.getLogger(Lexer.class.getName());
    private Reader reader = null;
    private int nextSymbol;
    private StringBuilder number = new StringBuilder("");

    public Lexer(Reader reader1) {
        try {
            reader = reader1;
            nextSymbol = reader.read();
        } catch (IOException e) {
            log.error(nextSymbol);
        }
    }

    Lexeme nextLexeme() throws WrongSymbolLexemeException {
        try {
            boolean isNumberBuild = false;
            //int a = '5';

            number.setLength(0);
            while (true) {
                int currentSymbol = nextSymbol;
                if (Character.isDigit(currentSymbol)) {
                    isNumberBuild = true;
                    number.append((char)currentSymbol);
                    nextSymbol = reader.read();
                }
                else {
                    if (isNumberBuild)
                        return new Lexeme(TerminalSymbols.Number, Integer.parseInt(number.toString()));
                    if (-1 == nextSymbol)
                        break;
                    nextSymbol = reader.read();
                    switch (currentSymbol) {
                        case '(': return new Lexeme(TerminalSymbols.LeftBracket);
                        case ')': return new Lexeme(TerminalSymbols.RightBracket);
                        case '+': return new Lexeme(TerminalSymbols.Plus);
                        case '-': return new Lexeme(TerminalSymbols.Minus);
                        case '/': return new Lexeme(TerminalSymbols.Division);
                        case '*': return new Lexeme(TerminalSymbols.Multiplication);
                        case '^': return new Lexeme(TerminalSymbols.Involution);
                        default:
                            throw new WrongSymbolLexemeException();
                    }
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
           // System.out.println("Error");
        }
        return null;
    }
}
