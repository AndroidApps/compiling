package ru.nsu.fit.g14201.lipatkin;

/**
 * Created by castiel on 14.02.2017.d
 */
public class Parser {
    private Lexer lexer;
    private Lexeme next;

    public Parser(Lexer lexer1) {
        lexer = lexer1;
        next = lexer.nextLexeme();
    }

    public int parseAtom() {
        int result = 0;

        Lexeme current = next;
        if (null == current)
            throw new ParseException();
        next = lexer.nextLexeme();
        switch (current.getSymbol()) {
            case LeftBracket:
                result = parseExpression();
                current = next;
                if (null == current || TerminalSymbols.RightBracket != current.getSymbol())
                    throw new ParseException();
                next = lexer.nextLexeme();
                break;
            case Number:
                result = current.getValue();
                break;
            default:
                throw new ParseException();
        }

        return result;
    }

    public int parsePower() {
        if (next.getSymbol() == TerminalSymbols.Minus) {
            next = lexer.nextLexeme();
            return -parseAtom();
        }
        return parseAtom();
    }

    public int parseFactor() {
        int result = parsePower();

        if (next != null && next.getSymbol() == TerminalSymbols.Involution) {
            next = lexer.nextLexeme();
            int degree = parseFactor();
            int power = result;
            if (degree > 0)
                for (int i = 0; i < degree - 1; i++)
                    result *= power;
            else if (degree == 0)
                result = 1;
        }

        return result;
    }

    public int parseTerm() {
        int result = parseFactor();
        TerminalSymbols symbol;
        while (true) {
            if (next == null)
                break;
            symbol = next.getSymbol();
            if (symbol == TerminalSymbols.Multiplication) {
                next = lexer.nextLexeme();
                result *= parseFactor();
            }
            else if (symbol == TerminalSymbols.Division) {
                next = lexer.nextLexeme();
                result /= parseFactor();
            } else
                break;
        }
        return result;
    }

    public int parseExpression() {
        int result = parseTerm();
        TerminalSymbols symbol;
        while (true) {
            if (next == null)
                break;
            symbol = next.getSymbol();
            if (symbol == TerminalSymbols.Plus) {
                next = lexer.nextLexeme();
                result += parseTerm();
            }
            else if (symbol == TerminalSymbols.Minus) {
                next = lexer.nextLexeme();
                result -= parseTerm();
            }
            else
                break;
        }
        return result;
    }
}
