package ru.nsu.fit.g14201.lipatkin;

/**
 * Created by castiel on 13.02.2017.
 */
public class WrongSymbolLexemeException extends CalculatorException {
    public String getMessage() {
        return "Lexeme Exception: wrong symbol";
    }
}
